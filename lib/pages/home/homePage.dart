import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? email;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initLocationFromPref();
    initGetEmailFromPref();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  initLocationFromPref() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final location = prefs.getString("location");
    print("ini halaman dashboard - Home");
    print(location);
  }

  initGetEmailFromPref() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final emails = prefs.getString("email");
    setState(() {
      email = emails.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              profileAkun(),
              menuList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget profileAkun() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Image.network(
              "https://images.rawpixel.com/image_800/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvcm0yMjItbWluZC0xNl8xLmpwZw.jpg",
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 32.0,
            ),
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("DUMMY INDONESIA"),
                SizedBox(height: 22),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Container(
                          width: 135,
                          height: 135,
                          child: Image.network(
                            "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
                          ),
                        ),
                      ),
                      SizedBox(width: 28),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Nama Karyawan"),
                            SizedBox(height: 8),
                            Text(email != null ? email.toString() : "None"),
                            SizedBox(height: 8),
                            Text("IT Programmer")
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget menuList() {
    return Container(
      margin: EdgeInsets.only(top: 16.0),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 150,
                  height: 120,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.timer, size: 38),
                        SizedBox(height: 8),
                        Text("Absen Online"),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 24),
                Container(
                  width: 150,
                  height: 120,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.list_alt, size: 38),
                        SizedBox(height: 8),
                        Text("Riwayat Absen"),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 150,
                  height: 120,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.more_time, size: 38),
                        SizedBox(height: 8),
                        Text("SPK Lembur"),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 24),
                Container(
                  width: 150,
                  height: 120,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.calendar_month, size: 38),
                        SizedBox(height: 8),
                        Text("Izin"),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 150,
                  height: 120,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.cloud, size: 38),
                        SizedBox(height: 8),
                        Text("Cuti"),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 24),
                Container(
                  width: 150,
                  height: 120,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.local_hospital, size: 38),
                        SizedBox(height: 8),
                        Text("Sakit"),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
