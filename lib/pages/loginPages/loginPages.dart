import 'package:eabsenapp/pages/dashbord/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/karyawanModel.dart';
import 'package:geolocator/geolocator.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final dio = Dio();
  final _formKey = GlobalKey<FormState>();
  final _username = TextEditingController();
  final _password = TextEditingController();
  bool isBio = false;

  final LocalAuthentication auth = LocalAuthentication();
  bool? _canCheckBiometrics;
  List<BiometricType>? _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;
  bool authenticated = false;
  String lat = "";
  String long = "";

  @override
  void initState() {
    super.initState();
    initLocation();
    initalization();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initLocation() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    SchedulerBinding.instance.addPostFrameCallback((_) async {
      bool cekPermissionLocation = await handleLocationPermission(context);
      if (cekPermissionLocation) {
        await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high,
        ).then((Position position) async {
          print("latitude " + position.latitude.toString());
          print("longitude " + position.longitude.toString());
          await prefs.setString(
              "location", "${position.latitude}, ${position.longitude}");

          lat = position.latitude.toString();
          long = position.longitude.toString();
        });
      }
    });
  }

  Future<bool> handleLocationPermission(context) async {
    LocationPermission permission;

    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("DISABLED"),
        ),
      );
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("DENIED"),
          ),
        );
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("PERMANENT"),
        ),
      );
      return false;
    }
    return true;
  }

  initalization() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    print("PANGGIL PERTAMA KALI");

    final isBioCekfromPref = prefs.getBool("biometric");
    final token = prefs.getString("token");

    if (token != null) {
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (context) => const DashboardPage(),
        ),
        (Route<dynamic> route) => false,
      );
    }

    print("initialization $isBioCekfromPref");
    if (isBioCekfromPref != null) {
      if (isBioCekfromPref) {
        setState(() {
          isBio = isBioCekfromPref;
        });
      }
    }
  }

  btnSubmit() async {
    if (_formKey.currentState!.validate()) {
      Map<String, dynamic> datas = {
        "email": _username.text.toString(),
        "password": _password.text.toString(),
        "latitude": lat.toString(),
        "longitude": long.toString()
      };
      print(datas);

      await hitapiLogin(datas);
    }
  }

  hitapiLogin(datas) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    // hit api
    try {
      final response = await dio.post(
        'https://cek-wa.com/presensi/public/api/login',
        data: datas,
      );
      print(response.data);
      Karyawan dataKaryawan = Karyawan.fromJson(response.data);
      print(dataKaryawan);

      await prefs.setBool('biometric', true);
      await prefs.setString("email", dataKaryawan.data.email);
      await prefs.setString("token", dataKaryawan.data.token);
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (context) => const DashboardPage(),
        ),
        (Route<dynamic> route) => false,
      );
    } on DioException catch (e) {
      if (e.response != null) {
        print(e.response!.data);
        print(e.response!.headers);
        print(e.response!.requestOptions);
      } else {
        print(e.requestOptions);
        print(e.message);
      }
    }
  }

  cekBiometric() async {
    late bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      canCheckBiometrics = false;
      print(e);
    }
    if (!mounted) {
      return;
    }

    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });

    if (_canCheckBiometrics!) {
      _getAvailableBiometrics();
    }
  }

  Future<void> _getAvailableBiometrics() async {
    late List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      availableBiometrics = <BiometricType>[];
      print(e);
    }
    if (!mounted) {
      return;
    }

    setState(() {
      _availableBiometrics = availableBiometrics;
    });

    if (_availableBiometrics!.length > 0) {
      _authenticateWithBiometrics();
    }
  }

  Future<void> _authenticateWithBiometrics() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });

      authenticated = await auth.authenticate(
        localizedReason:
            'Scan your fingerprint (or face or whatever) to authenticate',
        options: const AuthenticationOptions(
          stickyAuth: true,
          biometricOnly: true,
        ),
      );

      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });

      if (authenticated) {
        setState(() {
          _username.text = prefs.getString("email").toString();
        });
      }
    } on PlatformException catch (e) {
      print(e);
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Error - ${e.message}';
      });
      return;
    }
    if (!mounted) {
      return;
    }

    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    setState(() {
      _authorized = message;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Login"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: const Icon(
                    Icons.assignment_ind,
                    size: 160,
                    color: Colors.red,
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  child: const Text(
                    "Dummy Indonesia",
                    style: TextStyle(fontSize: 26),
                  ),
                ),
                SizedBox(height: 32),
                Container(
                  child: TextFormField(
                    controller: _username,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'ID Karyawan atau Email',
                      labelText: 'ID Karyawan atau Email',
                      prefixIcon: Icon(Icons.account_circle, color: Colors.red),
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 14.0,
                        horizontal: 5.0,
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'ID Karyawan atau Email tidak boleh kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(height: 12),
                Container(
                  child: TextFormField(
                    controller: _password,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Password',
                      labelText: 'Password',
                      prefixIcon: Icon(Icons.lock, color: Colors.red),
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 14.0,
                        horizontal: 5.0,
                      ),
                    ),
                    obscureText: true,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Password tidak boleh kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                isBio ? SizedBox(height: 16) : Container(),
                isBio
                    ? Container(
                        child: IconButton(
                          icon: Icon(Icons.fingerprint, size: 36),
                          onPressed: () {
                            cekBiometric();
                          },
                        ),
                      )
                    : SizedBox(),
                SizedBox(height: 32),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.red,
                      primary: Colors.white,
                    ),
                    onPressed: () {
                      btnSubmit();
                    },
                    child: Text('SUBMIT'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
